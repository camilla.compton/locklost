`locklost`: aLIGO IFO lock loss tracking and analysis
========================================================

This package provides a set of tools for analyzing LIGO detector "lock
losses".  It consists of four main components:

* `search` for detector lock losses in past data based on guardian
  state transitions.
* `analyze` individual lock losses to generate plots and look for
  identifying features.
* `online` search to monitor for lock losses and automatically run
  follow-up analyses.
* `web` interface to view lock loses event pages.

The `locklost` command line interface provides access to all of the
above functionality.

The LIGO sites have `locklost` deployments that automatically find and
analyze lock loss events.  The site lock loss web pages are available
at the following URLs:

* [H1: https://ldas-jobs.ligo-wa.caltech.edu/~lockloss/](https://ldas-jobs.ligo-wa.caltech.edu/~lockloss/)
* [L1: https://ldas-jobs.ligo-la.caltech.edu/~lockloss/](https://ldas-jobs.ligo-la.caltech.edu/~lockloss/)

If you notice any issues please [file a bug
report](https://git.ligo.org/jameson.rollins/locklost/-/issues), or if
you have any questions please contact the ["lock-loss" group on
chat.ligo.org](https://chat.ligo.org/ligo/channels/lock-loss).


# Analysis plugins

Lock loss event analysis is handled by a set of [analysis
"plugins"](/locklost/plugins/).  Each plugin is registered in
[`locklost/plugins/__init__.py`](/locklost/plugins/__init__.py).  Some
of the currently enabled plugins are:

* `discover.discover_data` wait for data to be available
* `refine.refine_event` refine event time
* `saturations.find_saturations` find saturating channels before event
* `lpy.find_lpy` find length/pitch/yaw oscillations in suspensions
* `glitch.analyze_glitches` look for glitches around event
* `overflows.find_overflows` look for ADC overflows
* `state_start.find_lock_start` find the start the lock leading to
  current lock loss

Each plugin does it's own analysis, although some depend on the output
of other plugins.  The output from any plugin (e.g. plots or data)
should be written into the event directory.


# Site deployments

Each site (LHO and LLO) has a dedicated "lockloss" account on their
local LDAS cluster where `locklost` is deployed.  The `locklost`
command line interface is available when logging into these acocunts.

NOTE: these accounts are for the managed `locklost` deployments
**ONLY**.  These are specially configured.  Please don't change the
configuration or run other jobs in these accounts unless you know what
you're doing.

The online searches and other periodic maintenance tasks are handled
via `systemd --user` on the dedicated "detchar.ligo-?a.caltech.edu"
nodes.  `systemd --user` is a user-specific instance of the
[systemd](https://systemd.io/) process supervision daemon.  The [arch
linux wiki](https://wiki.archlinux.org/title/Systemd) has a nice intro
to systemd.


## online systemd service

The online search service is called `locklost-online.service`.  To
control and view the status of the process use the `sysctemctl`
command, e.g.:
```shell
$ systemctl --user status locklost-online.service
$ systemctl --user start locklost-online.service
$ systemctl --user restart locklost-online.service
$ systemctl --user stop locklost-online.service
```

To view the logs from the online search user `journalctl`:
```shell
$ journalctl --user-unit locklost-online.service -f
```
The `-f` options tells journalctl to "follow" the logs and print new
log lines as they come in.

The "service unit" files for the online search service (and the timer
services described below) that describe how the units should be
managed by systemd are maintained in the [support](/support/systemd)
directory in the git source.


## systemd timers

Instead of cron, the site deployments use `systemd timers` to handle
periodic background jobs.  The enabled timers and their schedules can
be viewed with:
```shell
$ systemctl --user list-timers
```

Timers are kind of like other services, which can be started/stopped,
enabled/disabled, etc.  Unless there is some issue they should usually
just be left alone.


## deploying new versions

When a new version is ready for release, create an annotated tag for
the release and push it to the [main
repo](https://git.ligo.org/jameson.rollins/locklost):
```shell
$ git tag -m release 0.22.1
$ git push --tags
```
In the "lockloss" account on the LDS "detchar" machines, pull the new
release and run the `deploy` script, which should automatically run the
tests before installing the new version:
```shell
$ ssh lockloss@detchar.ligo-la.caltech.edu
$ cd src/locklost
$ git pull
$ ./deploy
```
The deployment should automatically install any updates to the online
search, and restart the search service.


## common issues

There are a couple of issues that crop up periodically, usually due to
problems with the site LDAS clusters where the jobs run.


### online analysis restarting due to NDS problems

One of the most common problems is that the online analysis is falling
over because it can't connect to the site NDS server, for example:
```
2021-05-12_17:18:31 2021-05-12 17:18:31,317 NDS connect: 10.21.2.4:31200
2021-05-12_17:18:31 Error in write(): Connection refused
2021-05-12_17:18:31 Error in write(): Connection refused
2021-05-12_17:18:31 Traceback (most recent call last):
2021-05-12_17:18:31   File "/usr/lib64/python3.6/runpy.py", line 193, in _run_module_as_main
2021-05-12_17:18:31     "__main__", mod_spec)
2021-05-12_17:18:31   File "/usr/lib64/python3.6/runpy.py", line 85, in _run_code
2021-05-12_17:18:31     exec(code, run_globals)
2021-05-12_17:18:31   File "/home/lockloss/.local/lib/python3.6/site-packages/locklost-0.21.3-py3.6.egg/locklost/online.py", line 109, in <module>
2021-05-12_17:18:31     stat_file=stat_file,
2021-05-12_17:18:31   File "/home/lockloss/.local/lib/python3.6/site-packages/locklost-0.21.3-py3.6.egg/locklost/search.py", line 72, in search_iterate
2021-05-12_17:18:31     for bufs in data.nds_iterate([channel], start_end=segment):
2021-05-12_17:18:31   File "/home/lockloss/.local/lib/python3.6/site-packages/locklost-0.21.3-py3.6.egg/locklost/data.py", line 48, in nds_iterate
2021-05-12_17:18:31     with closing(nds_connection()) as conn:
2021-05-12_17:18:31   File "/home/lockloss/.local/lib/python3.6/site-packages/locklost-0.21.3-py3.6.egg/locklost/data.py", line 28, in nds_connection
2021-05-12_17:18:31     conn = nds2.connection(HOST, PORT)
2021-05-12_17:18:31   File "/usr/lib64/python3.6/site-packages/nds2.py", line 3172, in __init__
2021-05-12_17:18:31     _nds2.connection_swiginit(self, _nds2.new_connection(*args))
2021-05-12_17:18:31 RuntimeError: Failed to establish a connection[INFO: Error occurred trying to write to socket]
```

This is usually because the NDS server itself has died and needs to be
restarted/reset (frequently due to Tuesday maintenance).
Unfortunately the site admins aren't necessarily aware of this issue
and need to be poked about it.  Once the NDS server is back the job
should just pick up on it's own.


### analyze jobs failing because of cluster data problems

Another common failure mode is failing follow-up analysis jobs due to
data access problems in the cluster.  These often occur during Tuesday
maintenance, but often mysteriously at other times as well.  These
kinds of failures are indicated by the following exceptions in the
event analyze log:
```
2021-05-07 16:02:51,722 [analyze.analyze_event] exception in discover_data:
Traceback (most recent call last):
  File "/home/lockloss/.local/lib/python3.6/site-packages/locklost-0.21.0-py3.6.egg/locklost/analyze.py", line 56, in analyze_event
    func(event)
  File "/home/lockloss/.local/lib/python3.6/site-packages/locklost-0.21.0-py3.6.egg/locklost/plugins/discover.py", line 67, in discover_data
    raise RuntimeError("data discovery timeout reached, data not found")
RuntimeError: data discovery timeout reached, data not found
```
or:
```
2021-05-07 16:02:51,750 [analyze.analyze_event] exception in find_previous_state:
Traceback (most recent call last):
  File "/home/lockloss/.local/lib/python3.6/site-packages/locklost-0.21.0-py3.6.egg/locklost/analyze.py", line 56, in analyze_event
    func(event)
  File "/home/lockloss/.local/lib/python3.6/site-packages/locklost-0.21.0-py3.6.egg/locklost/plugins/history.py", line 26, in find_previous_state
    gbuf = data.fetch(channels, segment)[0]
  File "/home/lockloss/.local/lib/python3.6/site-packages/locklost-0.21.0-py3.6.egg/locklost/data.py", line 172, in fetch
    bufs = func(channels, start, stop)
  File "/home/lockloss/.local/lib/python3.6/site-packages/locklost-0.21.0-py3.6.egg/locklost/data.py", line 150, in frame_fetch_gwpy
    data = gwpy.timeseries.TimeSeriesDict.find(channels, start, stop, frametype=config.IFO+'_R')
  File "/usr/lib/python3.6/site-packages/gwpy/timeseries/core.py", line 1291, in find
    on_gaps="error" if pad is None else "warn",
  File "/usr/lib/python3.6/site-packages/gwpy/io/datafind.py", line 335, in wrapped
    return func(*args, **kwargs)
  File "/usr/lib/python3.6/site-packages/gwpy/io/datafind.py", line 642, in find_urls
    on_gaps=on_gaps)
  File "/usr/lib/python3.6/site-packages/gwdatafind/http.py", line 433, in find_urls
    raise RuntimeError(msg)
RuntimeError: Missing segments: 
[1304463181 ... 1304463182)
```

This problem sometimes just corrects itself, but often needs admin
poking as well.


## back-filling events

After things have recovered from any of the issues mentioned above,
you'll probably want to back-fill any missed events.  The best way to
do that is to run a condor `search` for missed event (e.g. from "4
weeks ago" until "now"):
```shell
$ locklost search --condor '4 weeks ago' now
```

The search will find lock loss events, but it will not run the
follow-up analyses.  To run the full analysis on any un-analyzed
events, run the condor `analyze` command over the same time range,
e.g.:
```shell
$ locklost analyze --condor '4 weeks ago' now
```
The `analyze` command will analyze both missed/new events but also
"failed" events.

You can also re-analyze a specific event with the `--rerun` flag:
```shell
$ locklost analyze TIME --rerun
```
or
```shell
$ locklost analyze --condor START END --rerun
```

Occaissionally analysis jobs are improperly killed by condor, not
giving them a chance to clean up their run locks.  The find and remove
any old, stale analysis locks:
```shell
$ locklost find-locks -r
```

We also have timer services available to handle the backfilling after
Tuesday maintenance if need be:
```shell
$ systemctl --user enable --now locklost-search-backfill.timer
$ systemctl --user enable --now locklost-analyze-backfill.timer
```


# Developing and contributing

See the [CONTRIBUTING](CONTRIBUTING.md) for instructions on how to
contribute to `locklost`.
