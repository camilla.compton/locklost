import argparse

from .. import set_signal_handlers
from .. import config_logger
from ..event import LocklossEvent
from . import PLUGINS


parser = argparse.ArgumentParser()
parser.add_argument(
    'plugin', nargs='?',
)
parser.add_argument(
    'event', nargs='?',
)


def main():
    set_signal_handlers()
    config_logger()
    args = parser.parse_args()
    if not args.plugin:
        for name, func in PLUGINS.items():
            print(name)
        return
    if not args.event:
        parser.error("must specifiy event")
    try:
        func = PLUGINS[args.plugin]
    except KeyError:
        parser.error(f"unknown plugin: {args.plugin}")
    event = LocklossEvent(args.event)
    func(event)


if __name__ == '__main__':
    main()
