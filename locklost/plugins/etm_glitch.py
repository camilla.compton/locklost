import numpy as np
import matplotlib.pyplot as plt
from scipy import signal

from gwpy.segments import Segment

from .. import logger
from .. import config
from .. import data
from .. import plotutils

GLITCH_SEARCH_WINDOW = [-2.0, 0]
GLITCH_CHANNEL = ['{}:SUS-ETMX_L3_MASTER_OUT_LR_DQ'.format(config.IFO)]
THRESH = 2 * (10 ** 4)  # High threshold looking for glitches
LOW_THRESH = 2 * (10 ** 4)  # Low threshold for number of zeroes
LOW_TIME = 50 * (10 ** -3)  # Threshold for amount of time spent within LOW_GLITCH threshold


##############################################


def check_glitch(event):
    """
    This will use a 200Hz high-passed verion of GLITCH_CHANNEL to check for
    any glitches prior to lockloss in two steps:
    1. Is there a glitch that goes above THRESH in the GLITCH_SEARCH_WINDOW
    If this is true:
    2. Does the channel remain within LOW_THRESH for at least LOW_TIME
    """
    if config.IFO == 'L1':
        logger.info("NOT SET UP FOR LLO.")
        return

    if event.transition_index[0] < config.GRD_NOMINAL_STATE[0]:
        logger.info("IFO not fully locked, plugin will not run.")
        return

    plotutils.set_rcparams()
    mod_window = [GLITCH_SEARCH_WINDOW[0], GLITCH_SEARCH_WINDOW[1]]
    segment = Segment(mod_window).shift(event.gps)

    buf = data.fetch(GLITCH_CHANNEL, segment)[0]
    thresh_crossing = segment[1]
    srate = buf.sample_rate
    t = np.arange(segment[0], segment[1], 1 / srate)
    highpass = signal.butter(4, 200, btype='highpass', output='sos', fs=srate)
    hp_data = signal.sosfilt(highpass, buf.data)

    if event.refined_gps:
        lockloss_time = event.refined_gps
    else:
        lockloss_time = event.gps

    thresh_ref = hp_data[np.where(t < lockloss_time - LOW_TIME)]
    glitches = np.where((thresh_ref <= -THRESH) | (thresh_ref >= THRESH))[0]
    # discard first 100ms of data to avoid filtering edge effects
    glitches = glitches[glitches > 0.1 * srate]

    if not any(glitches):
        logger.info("No glitches found in {}".format(GLITCH_CHANNEL[0]))

    else:
        glitch_time = t[glitches[0]]
        logger.info("Threshold crossed at time: {}".format(glitch_time))
        low_ref = hp_data[np.where((t >= glitch_time) & (t < lockloss_time))]

        count = 0
        low_list = []
        for value in low_ref:
            if -LOW_THRESH < value < LOW_THRESH:
                count += 1
            else:
                # gone outside of LOW_THRESH threshold, add count to list and reset.
                low_list.append(count)
                count = 0
        low_list.append(count)

        thresh_crossing = min(glitch_time, thresh_crossing)
        low_max_time = max(low_list) / srate
        # Checking that the channel remained within LOW_THRESH (1e5) for at least LOW_TIME (60ms)
        logger.info("Channel remained for {}ms below threshold.".format(int(1000 * low_max_time)))
        if (low_max_time >= LOW_TIME):
            event.add_tag('ETM_GLITCH')
        else:
            logger.info('Channel did not remain within threshold, no ETM glitch found.')

    # Make plots
    fig, ax = plt.subplots(1, figsize=(22, 16))

    ax.plot(
        t - event.gps,
        buf.data,
        label=buf.channel,
        alpha=0.8,
        lw=2,
    )

    ax.grid()
    ax.set_xlabel('Time [s] since lock loss at {}'.format(event.gps), labelpad=10)
    ax.set_ylabel('Counts')
    ax.legend(loc='best')
    ax.set_title('Glitch check', y=1.04)
    ax.set_xlim(segment[0] - event.gps, segment[1] - event.gps)
    ax.set_ylim(-3.9 * (10 ** 5), 3.9 * (10 ** 5))
    if thresh_crossing != segment[1]:
        plotutils.set_thresh_crossing(ax, thresh_crossing, event.gps, segment)

    fig.tight_layout()
    fig.savefig(event.path('ETM_GLITCH.png'))

    # Make High Passed data plot
    fig1, ax1 = plt.subplots(1, figsize=(22, 16))

    ax1.plot(
        t - event.gps,
        hp_data,
        label='200Hz HP of {}'.format(buf.channel),
        alpha=0.8,
        lw=2,
    )
    ax1.axhline(
        -THRESH,
        linestyle='--',
        color='black',
        label='Glitch threshold',
        lw=5,
    )
    ax1.axhline(
        THRESH,
        linestyle='--',
        color='black',
        lw=5,
    )
    ax1.grid()
    ax1.set_xlabel('Time [s] since lock loss at {}'.format(event.gps), labelpad=10)
    ax1.set_ylabel('Counts')
    ax1.legend(loc='best')
    ax1.set_title('Glitch check, 200Hz High Passed', y=1.04)
    ax1.set_xlim(segment[0] - event.gps, segment[1] - event.gps)
    ax1.set_ylim(1.9 * -THRESH, 1.9 * THRESH)
    if thresh_crossing != segment[1]:
        plotutils.set_thresh_crossing(ax1, thresh_crossing, event.gps, segment)

    fig1.tight_layout()
    fig1.savefig(event.path('ETM_GLITCH_HP.png'))
